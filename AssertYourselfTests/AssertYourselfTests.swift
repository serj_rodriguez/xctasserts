//
//  AssertYourselfTests.swift
//  AssertYourselfTests
//
//  Created by 64451 on 30/11/22.
//

import XCTest
@testable import AssertYourself

final class AssertYourselfTests: XCTestCase {

    func test_fail(){
        XCTFail()
    }
    
    func test_fail_withSimpleMessage(){
        XCTFail("We have a problem")
    }
    
    func test_fail_withInterpolatedMessage(){
        let theAnswer = 42
        XCTFail("The answer to the Great Question is \(theAnswer)")
    }
    
    func test_assertTrue(){
        let success = false
        XCTAssertTrue(success)
    }
    
    func test_AssertNil(){
        let optionalValue : Int? = 123
        XCTAssertNil(optionalValue)
    }
    
    struct SimpleStruct: CustomStringConvertible{
        let x: Int
        let y: Int
        var description: String{ "(\(x), \(y)" }
    }
    
    func test_assertNil_withSimpleStruct(){
        let optionalValue: SimpleStruct? = SimpleStruct(x: 1, y: 2)
        XCTAssertNil(optionalValue)
    }
    
    func test_assertEqual(){
        let actual = "actual"
        XCTAssertEqual(actual, "expected")
    }
    
    func test_assertEqual_withOptional(){
        let result: String? = "foo"
        XCTAssertEqual(result, "bar")
    }
    
    func test_floatingPointDanger(){
        let result = 0.1 + 0.2
        XCTAssertEqual(result, 0.3)
    }
    
    //Use accuracy only with working with assertEqual and Double or Float types
    func test_floatingPointFixed(){
        let result = 0.1 + 0.2
        XCTAssertEqual(result, 0.3, accuracy: 0.0001)
    }
    
    func test_messageOverkill(){
        let actual = "actual"
        XCTAssertEqual(actual, "expected", "Expected \"expected\" but got \"\(actual)\"")
    }
}
